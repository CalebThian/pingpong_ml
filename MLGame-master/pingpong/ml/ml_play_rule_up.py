"""
The template of the script for the machine learning process in game pingpong
"""

# Import the necessary modules and classes
import pingpong.communication as comm
from pingpong.communication import (
	SceneInfo, GameInstruction, GameStatus, PlatformAction
)

def ml_loop(side: str):
	"""
	The main loop for the machine learning process

	The `side` parameter can be used for switch the code for either of both sides,
	so you can write the code for both sides in the same script. Such as:
	```python
	if side == "1P":
		ml_loop_for_1P()
	else:
		ml_loop_for_2P()
	```

	@param side The side which this script is executed for. Either "1P" or "2P".
	"""

	# === Here is the execution order of the loop === #
	# 1. Put the initialization code here
    ball_last_y=-1
    ball_last_x=-1
    it = 20
	# 2. Inform the game process that ml process is ready
	comm.ml_ready()

	# 3. Start an endless loop
	while True:
		# 3.1. Receive the scene information sent from the game process
		scene_info = comm.get_scene_info()
        ball_now_x=scene_info.ball[0]
        ball_now_y=scene_info.ball[1]
        des=scene_info.ball[0]+3

		# 3.2. If either of two sides wins the game, do the updating or
		#      reseting stuff and inform the game process when the ml process
		#      is ready.
		if scene_info.status == GameStatus.GAME_1P_WIN or \
		   scene_info.status == GameStatus.GAME_2P_WIN:
			# Do something updating or reseting stuff

        if ball_last_x==-1 and ball_last_y==-1:
            if ball_now_x==75:
                ball_last_x=ball_now_x-7
                ball_last_y=ball_now_y-7
            else:
                ball_last_x=ball_now_x+7
                ball_last_y=ball_now_y+7
                

        if side=="1P":
            if ball_last_y-ball_now_y<0:
                times=int((395-ball_now_y)/(ball))
                if ball_last_x-ball_now_x>0:
                    lr=0
                else :
                    lr=1
            for i in range(0,times):
                if lr==0 and des-7<=0:
                    lr=1
                    des+=7
                elif lr==0:
                    des-=7
                elif lr==1 and des+7>=200:
                    lr=0
                    des-=7
                else:
                    des+=7

			# 3.2.1 Inform the game process that
			#       the ml process is ready for the next round
			comm.ml_ready()
			continue

		# 3.3 Put the code here to handle the scene information
        if scene_info.platform[0]+it<des:
            comm.send_instruction(scene_info.frame, GameInstruction.CMD_RIGHT)
        elif scene_info.platform[0]+it>des:
            comm.send_instruction(scene_info.frame, GameInstruction.CMD_LEFT)
        else:
            comm.send_instruction(scene_info.frame, GameInstruction.CMD_NONE) 


		# 3.4 Send the instruction for this frame to the game process
		comm.send_instruction(scene_info.frame, PlatformAction.MOVE_LEFT)
