# -*- coding: utf-8 -*-
"""
Created on Wed Jun  5 22:08:05 2019

@author: Caleb Thian
"""

"""
The template of the script for the machine learning process in game pingpong
"""

# Import the necessary modules and classes
import pingpong.communication as comm
from pingpong.communication import (
	SceneInfo, GameInstruction, GameStatus, PlatformAction
)
import os.path
import numpy as np
import pickle

def ml_loop(side: str):
	"""
	The main loop for the machine learning process

	The `side` parameter can be used for switch the code for either of both sides,
	so you can write the code for both sides in the same script. Such as:
	```python
	if side == "1P":
		ml_loop_for_1P()
	else:
		ml_loop_for_2P()
	```

	@param side The side which this script is executed for. Either "1P" or "2P".
	"""

	# === Here is the execution order of the loop === #
	# 1. Put the initialization code here
	# 2. Inform the game process that ml process is ready
	filename="rfc_"+side+".sav" 
	ld3=pickle.load(open(filename,'rb'))
	comm.ml_ready()

	# 3. Start an endless loop
	if side=="1P":
		ball_last_x=-1
		ball_last_y=-1
		ball_speed_x=7
		ball_speed_y=7
		while True:
		# 3.1. Receive the scene information sent from the game process
			scene_info = comm.get_scene_info()
			ball_now_x=scene_info.ball[0]
			ball_now_y=scene_info.ball[1]
			
			if ball_last_x != -1:
				ball_speed_x=ball_now_x-ball_last_x
				ball_speed_y=ball_now_y-ball_last_y
			elif ball_now_x==75:
				ball_speed_x=7
				ball_speed_y=7
			else:
				ball_speed_x=-7
				ball_speed_y=-7
			ball_last_x=ball_now_x
			ball_last_y=ball_now_y
			des=scene_info.ball[0]
			height=scene_info.ball[1]
			f=scene_info.frame
			speed=scene_info.ball_speed
			while height>80:
				height+=ball_speed_y
				if height>=420:
					ball_speed_y=-ball_speed_y
					if height+ball_speed_y>=420:
						break
				des+=ball_speed_x
				if des<=0 or des>=200:
					ball_speed_x=-ball_speed_x
				f+=1
				if f%200==0:
					speed+=1
					f=0
				if ball_speed_x<0:
					ball_speed_x=-speed
				else:
					ball_speed_x=speed
				if ball_speed_y<0:
					ball_speed_y=-speed
				else:
					ball_speed_y=speed
			inp_temp=np.array([scene_info.ball[0],scene_info.ball[1],scene_info.platform_1P[0]+20,des])
			input = inp_temp[np.newaxis,:] 
		# 3.2. If either of two sides wins the game, do the updating or
		#      reseting stuff and inform the game process when the ml process
		#      is ready.
			if scene_info.status == GameStatus.GAME_1P_WIN or \
				scene_info.status == GameStatus.GAME_2P_WIN:
				# Do something updating or reseting stuff
				ball_last_x=-1
				ball_last_y=-1
				ball_speed_x=7
				ball_speed_y=7
				# 3.2.1 Inform the game process that
				#       the ml process is ready for the next round
				comm.ml_ready()
				continue

		# 3.3 Put the code here to handle the scene information
		
		# 3.4 Send the instruction for this frame to the game process
			if ld3.predict(input)==1:
				comm.send_instruction(scene_info.frame, PlatformAction.MOVE_RIGHT)
			elif ld3.predict(input)==-1:
				comm.send_instruction(scene_info.frame, PlatformAction.MOVE_LEFT)
			else:
				comm.send_instruction(scene_info.frame, PlatformAction.NONE)
	else:
		ball_last_x=-1
		ball_last_y=-1
		ball_speed_x=7
		ball_speed_y=7
		while True:
		# 3.1. Receive the scene information sent from the game process
			scene_info = comm.get_scene_info()
			ball_now_x=scene_info.ball[0]
			ball_now_y=scene_info.ball[1]
			if ball_last_x != -1:
				ball_speed_x=ball_now_x-ball_last_x
				ball_speed_y=ball_now_y-ball_last_y
			elif ball_now_x==75:
				ball_speed_x=7
				ball_speed_y=7
			else:
				ball_speed_x=-7
				ball_speed_y=-7
			ball_last_x=ball_now_x
			ball_last_y=ball_now_y
			des=scene_info.ball[0]
			height=scene_info.ball[1]
			f=scene_info.frame
			speed=scene_info.ball_speed
			while height<420:
				height+=ball_speed_y
				if height<=80:
					ball_speed_y=-ball_speed_y
					if height+ball_speed_y<=80:
						break
				des+=ball_speed_x
				if des<=0 or des>=195:
					ball_speed_x=-ball_speed_x
				f+=1
				if f%200==0:
					speed+=1
					f=0
				if ball_speed_x<0:
					ball_speed_x=-speed
				else:
					ball_speed_x=speed
				if ball_speed_y<0:
					ball_speed_y=-speed
				else:
					ball_speed_y=speed

			inp_temp=np.array([scene_info.ball[0],scene_info.ball[1],scene_info.platform_2P[0]+20,des])
			input = inp_temp[np.newaxis,:] 
		# 3.2. If either of two sides wins the game, do the updating or
		#      reseting stuff and inform the game process when the ml process
		#      is ready.
			if scene_info.status == GameStatus.GAME_1P_WIN or \
				scene_info.status == GameStatus.GAME_2P_WIN:
				# Do something updating or reseting stuff
				ball_last_x=-1
				ball_last_y=-1
				ball_speed_x=7
				ball_speed_y=7
				# 3.2.1 Inform the game process that
				#       the ml process is ready for the next round
				comm.ml_ready()
				continue

		# 3.3 Put the code here to handle the scene information

		# 3.4 Send the instruction for this frame to the game process
			if ld3.predict(input)==1:
				comm.send_instruction(scene_info.frame, PlatformAction.MOVE_RIGHT)
			elif ld3.predict(input)==-1:
				comm.send_instruction(scene_info.frame, PlatformAction.MOVE_LEFT)
			else:
				comm.send_instruction(scene_info.frame, PlatformAction.NONE)