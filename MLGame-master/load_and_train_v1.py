# -*- coding: utf-8 -*-
"""
Created on Wed Jun  5 17:26:05 2019

@author: Caleb Thian
"""

import pickle
with open("2019-06-05_17-26-20.pickle", "rb") as f1:
    data_list1 = pickle.load(f1)
with open("2019-06-05_17-28-13.pickle", "rb") as f2:
    data_list2 = pickle.load(f2)
with open("2019-06-05_17-30-09.pickle", "rb") as f3:
    data_list3 = pickle.load(f3)
with open("2019-06-05_17-32-02.pickle", "rb") as f4:
    data_list4 = pickle.load(f4)
with open("2019-06-05_17-33-58.pickle", "rb") as f5:
    data_list5 = pickle.load(f5)
# save each information seperately
Frame=[]
Status=[]
Ballposition=[]
Ballspeed=[]
PlatformPosition_1P=[]
PlatformPosition_2P=[]
for j in range(0,len(data_list1)):
	i=j%len(data_list1)
	Frame.append(data_list1[i].frame)
	Status.append(data_list1[i].status)
	Ballposition.append(data_list1[i].ball)
	Ballspeed.append(data_list1[i].ball_speed)
	PlatformPosition_1P.append(data_list1[i].platform_1P)
	PlatformPosition_2P.append(data_list1[i].platform_2P)

for j in range(0,len(data_list2)):
	i=j%len(data_list2)
	Frame.append(data_list2[i].frame)
	Status.append(data_list2[i].status)
	Ballposition.append(data_list2[i].ball)
	Ballspeed.append(data_list2[i].ball_speed)
	PlatformPosition_1P.append(data_list2[i].platform_1P)
	PlatformPosition_2P.append(data_list2[i].platform_2P)

for j in range(0,len(data_list3)):
	i=j%len(data_list3)
	Frame.append(data_list3[i].frame)
	Status.append(data_list3[i].status)
	Ballposition.append(data_list3[i].ball)
	Ballspeed.append(data_list3[i].ball_speed)
	PlatformPosition_1P.append(data_list3[i].platform_1P)
	PlatformPosition_2P.append(data_list3[i].platform_2P)

for j in range(0,len(data_list4)):
	i=j%len(data_list4)
	Frame.append(data_list4[i].frame)
	Status.append(data_list4[i].status)
	Ballposition.append(data_list4[i].ball)
	Ballspeed.append(data_list4[i].ball_speed)
	PlatformPosition_1P.append(data_list4[i].platform_1P)
	PlatformPosition_2P.append(data_list4[i].platform_2P)

for j in range(0,len(data_list5)):
	i=j%len(data_list5)
	Frame.append(data_list5[i].frame)
	Status.append(data_list5[i].status)
	Ballposition.append(data_list5[i].ball)
	Ballspeed.append(data_list5[i].ball_speed)
	PlatformPosition_1P.append(data_list5[i].platform_1P)
	PlatformPosition_2P.append(data_list5[i].platform_2P)

#%% calculate instruction of each frame using platformposition
import numpy as np
PlatX_1P=np.array(PlatformPosition_1P)[:,0][:, np.newaxis]
PlatX_1P_next=PlatX_1P[1:,:]
instruct_1P=(PlatX_1P_next-PlatX_1P[0:len(PlatX_1P_next),0][:,np.newaxis])/5

PlatX_2P=np.array(PlatformPosition_2P)[:,0][:, np.newaxis]
PlatX_2P_next=PlatX_2P[1:,:]
instruct_2P=(PlatX_2P_next-PlatX_2P[0:len(PlatX_2P_next),0][:,np.newaxis])/5

#Preprocess ball speed
BallX=np.array(Ballposition)[:,0][:,np.newaxis]
BallX_next=BallX[1:,:]
BallY=np.array(Ballposition)[:,1][:,np.newaxis]
BallY_next=BallY[1:,:]
BallX_speed=(BallX_next-BallX[0:len(BallX_next),0][:,np.newaxis])
BallY_speed=(BallY_next-BallY[0:len(BallY_next),0][:,np.newaxis])

#Preprocess platform center
PlatX_1P_center=(PlatX_1P+20)
PlatX_2P_center=(PlatX_2P+20)

#Preprocess Des
Des_1P=BallX[0:-1]
Des_2P=BallX[0:-1]
for i in range(0,len(Des_1P)):
	height=BallY[i]
	f=Frame[i]
	speed=Ballspeed[i]
	ball_speed_y=BallY_speed[i]
	ball_speed_x=BallX_speed[i]
	des=Des_1P[i]
	while height>80:
		height+=ball_speed_y
		if height>=420:
			ball_speed_y=-ball_speed_y
		if height+ball_speed_y>=420:
				break
		des+=ball_speed_x
		if des<=0 or des>=200:
			ball_speed_x=-ball_speed_x
		f+=1
		if f%200==0:
			speed+=1
			f=0
		if ball_speed_x<0:
			ball_speed_x=-speed
		else:
			ball_speed_x=speed
		if ball_speed_y<0:
			ball_speed_y=-speed
		else:
			ball_speed_y=speed
	Des_1P[i]=des

	height=BallY[i]
	f=Frame[i]
	speed=Ballspeed[i]
	ball_speed_y=BallY_speed[i]
	ball_speed_x=BallX_speed[i]
	des=Des_2P[i]
	while height<420:
		height+=ball_speed_y
		if height<=80:
			ball_speed_y=-ball_speed_y
		if height+ball_speed_y<=80:
			break
		des+=ball_speed_x
		if des<=0 or des>=195:
			ball_speed_x=-ball_speed_x
		f+=1
		if f%200==0:
			speed+=1
			f=0
		if ball_speed_x<0:
			ball_speed_x=-speed
		else:
			ball_speed_x=speed
		if ball_speed_y<0:
			ball_speed_y=-speed
		else:
			ball_speed_y=speed
	Des_2P[i]=des

# select some features to make x
Ballarray=np.array(Ballposition[:-1])
#frame=np.array(Frame)[:-1][:,np.newaxis]
#x=np.hstack((Rel,BallX_speed[1:],BallY_speed[1:]))
x_1P=np.hstack((Ballarray,PlatX_1P_center[0:-1,0][:,np.newaxis],Des_1P))
x_2P=np.hstack((Ballarray,PlatX_2P_center[0:-1,0][:,np.newaxis],Des_2P))
#x=np.hstack((Ballarray[1:], PlatX_center[0:-2,0][:,np.newaxis],Rel*7,BallX_speed[1:]*7,BallY_speed[1:]*7))
#x=np.hstack((Rel,Ballarray[1:],PlatX_center[0:-2,0][:,np.newaxis],BallX_speed[1:],BallY_speed[1:]))
# select intructions as y
y_1P=instruct_1P
y_2P=instruct_2P
#%% train your model here
# from xxx import ooo_model
from sklearn.neighbors import KNeighborsClassifier 
from sklearn.ensemble import RandomForestClassifier
from sklearn import svm
from sklearn.metrics import accuracy_score
from sklearn.model_selection import train_test_split
#from sklearn.datasets import make_classification

x_1P_train,x_1P_test,y_1P_train,y_1P_test = train_test_split(x_1P,y_1P,test_size=0.2,random_state=10)
x_2P_train,x_2P_test,y_2P_train,y_2P_test = train_test_split(x_2P,y_2P,test_size=0.2,random_state=10)
#x,y=make_classification(n_samples=1000,n_features=4,n_informative=2,n_redundant=0,random_state=0,shuffle=False)
rfc1=RandomForestClassifier(random_state=0)
rfc2=RandomForestClassifier(random_state=0)
rfc1.fit(x_1P_train,y_1P_train)
rfc2.fit(x_2P_train,y_2P_train)

knn1 = KNeighborsClassifier(n_neighbors=3)
knn2 = KNeighborsClassifier(n_neighbors=3)
knn1.fit(x_1P_train,y_1P_train)
knn2.fit(x_1P_train,y_1P_train)

s1 = svm.SVC(gamma=0.001,decision_function_shape='oaa')
s2 = svm.SVC(gamma=0.001,decision_function_shape='oaa')
s1.fit(x_1P_train,y_2P_train)
s2.fit(x_2P_train,y_2P_train)

y_1P_rfc=rfc1.predict(x_1P_test)
y_1P_knn=knn1.predict(x_1P_test)
y_1P_s=s1.predict(x_1P_test)

y_2P_rfc=rfc2.predict(x_2P_test)
y_2P_knn=knn2.predict(x_2P_test)
y_2P_s=s2.predict(x_2P_test)

acc_1P_rfc=accuracy_score(y_1P_rfc,y_1P_test)
acc_1P_knn=accuracy_score(y_1P_knn,y_1P_test)
acc_1P_s=accuracy_score(y_1P_s,y_1P_test)
acc_2P_rfc=accuracy_score(y_2P_rfc,y_2P_test)
acc_2P_knn=accuracy_score(y_2P_knn,y_2P_test)
acc_2P_s=accuracy_score(y_2P_s,y_2P_test)

#%% save model
import pickle

filename="rfc_1P.sav"
pickle.dump(rfc1,open(filename, 'wb'))
filename="knn_1P.sav"
pickle.dump(knn1,open(filename, 'wb'))
filename="svm_1P.sav"
pickle.dump(s1,open(filename, 'wb'))
filename="rfc_2P.sav"
pickle.dump(rfc2,open(filename, 'wb'))
filename="knn_2P.sav"
pickle.dump(knn2,open(filename, 'wb'))
filename="svm_2P.sav"
pickle.dump(s2,open(filename, 'wb'))
